# Showdown Tournament
It's the ultimate GR Dutchmen challenge! Pair up and take on your fellow teammates in a double elimination Heroes Vs. Villians Showdown tournament. Join your team's dedicated Discord channel to strategize and hang out in the general channel between matches.

# Prerequisites
- Star Wars Battlefront II for XBOX (duh)
- [Install Discord](https://discord.com/download) on you computer or cellphone
- [Join GR Dutchmen Discord server](https://discord.gg/S7Ggw7m)

# Special Rules
- Before the match begins, each player may ban the use of one character on the opposing team
- Players may select any unbanned character at will
- For the first tournament, teams are randomly selected
- After every tournament, each team swaps a random player with another as indicated below:
  - 1st place <--> 4th place
  - 2nd place <--> 3rd place

# Current Teams
Check out the [tournament history](pages/tournament-history.md)
## Red
- |TheSticker|
- |KingPat|
## Green
- |Ranger|7367
- |LilVader|
## Blue
- |Blummer|
- |Kennworth|
## Alpha
- SeaUnicornMagic
- |TheChief|
## Bench
- |PlayinHoeky|

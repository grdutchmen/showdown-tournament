import argparse
import functools
import json
import random

_TEAM_NAMES_ = [
    'Red',
    'Green',
    'Blue',
    'Alpha',
]

_PLAYERS_ = [
    # '|PlayinHoeky|',
    '|LilVader|',
    '|Blummer|',
    '|KingPat|',
    '|TheChief|',
    '|TheSticker|',
    'SeaUnicornMagic',
    '|Kennworth|',
    '|Ranger|7367',
]

if __name__ == '__main__':
    # Parse program inputs
    parser = argparse.ArgumentParser('generate_new_teams')
    parser.add_argument('-o', '--output-file', default='data/teams.json')
    parser.add_argument('-p', '--player-list', default='data/players.json')
    parser.add_argument('-t', '--team-name-list', default='data/team_names.json')
    args = parser.parse_args()

    # Import players
    players = json.load(open(args.player_list, 'r'))

    # Import team names
    team_names = json.load(open(args.team_name_list, 'r'))

    # Create teams
    random.shuffle(players)
    teams = [team for team in zip(players[::2], players[1::2])]

    # Save teams
    json.dump(
        dict((name, players) for name, players in zip(team_names, teams)),
        open(args.output_file, 'w'),
        indent=4,
    )
